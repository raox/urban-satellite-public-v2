import json
import torch
from torch.utils.data import DataLoader

from datasets.reg_dataset import RegDataset
from models.unet import Unet
from models.unet_variants import UnetVariant
from utils.metrics import mean_absolute_error, seg_scores, iou

from tqdm import tqdm


if __name__ == "__main__":
    data_file = "/data-nfs/sapar/data/list/citywise256_test.txt"
    data_location = "/data-nfs/sapar/data/citywise256"
    with open("/data-nfs/sapar/unet-v2/configs/remote_citywise.json", 'r') as f:
        config = json.load(f)

    dataset = RegDataset(data_file, data_location, config["norm"], None)
    loader = DataLoader(dataset, shuffle=False, batch_size=1)

    model_path = "/data-nfs/sapar/experiments/se_resnet50_256_sliding_400_22-04-2023_1/best_checkpoints/70epoch"
    model = UnetVariant(config["model"])
    
    model.load_state_dict(torch.load(model_path))
    model.eval()

    mae = 0
    mdice = 0
    ss_res = 0
    ss_tot = 0
    precision = 0
    recall = 0
    accuracy = 0
    mean_height = 0

    bins = [1, 4, 7, 10]
    bin_err = []
    sum_err = []
    for bin in range(len(bins) + 1) :
        bin_err.append(0)
        sum_err.append(0)

    count = 0
    # compute ground truth mean height
    for target in tqdm(loader):
        mean_height += target["orig_height"].sum() / torch.numel(target["orig_height"])

    mean_height = mean_height / len(loader)
    
    for target in tqdm(loader):
        image = target["image"].to(config["device"])
        pred = model(target["image"])
        pred_height = pred["height"].detach() * 400
        pred_cls = torch.softmax(pred["cls"], dim = 1).detach()
        pred_cls = torch.argmax(pred_cls, dim = 1)
        pred_height = (pred_height * pred_cls).cpu()
        target_height = target["orig_height"]

        # segmentation metrics
        tp, fp, tn, fn = seg_scores(pred_cls, target["cls"])
        precision += tp / (tp + fp)
        recall += tp / (tp + fn)
        accuracy += (tp + tn) / (tp + fp + tn + fn)
        mdice += (2 * tp) / (2 * tp + fp + fn)

        # regression metrics
        mae += mean_absolute_error(pred_height, target_height)
        ss_res += torch.sum((pred_height - target_height)**2)
        ss_tot += torch.sum((target_height - mean_height)**2)
        
        #metrics according to bins
        idxs = target_height == 0
        bin_err[0] += torch.sum(abs(pred_height[idxs] - target_height[idxs])) 
        sum_err[0] += torch.sum(idxs)
        
        for i in range(len(bins) - 1):
            start = bins[i]
            end = bins[i + 1]
            idxs = torch.logical_and(target_height >= start, target_height < end)

            bin_err[i+1] += torch.sum(abs(pred_height[idxs] - target_height[idxs]) / target_height[idxs])
            sum_err[i+1] += torch.sum(idxs)

        idxs = target_height >= bins[-1]
        bin_err[-1] += torch.sum(abs(pred_height[idxs] - target_height[idxs]) / target_height[idxs])
        sum_err[-1] += torch.sum(idxs)


    print("accuracy: ", accuracy / len(loader))
    print("precision: ", precision / len(loader))
    print("recall: ", recall / len(loader))
    print("dice: ", mdice / len(loader))
    print("MAE: ", mae / len(loader))
    print("R^2: ", 1 - (ss_res / ss_tot))    

    for i in range(len(bin_err)):
        print("bin_mre ", i, bin_err[i] / sum_err[i])
    
    



