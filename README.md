# Building Floorspace China
This is the code repository for the paper titled "Evolving Cityscape: A Dataset for Building Footprints and Heights from Satellite Imagery in China" (under review in Scientific Data). 

Preprint of the previous version of the paper is available under [this ArXiv link](https://arxiv.org/abs/2303.02230).

# Project tree
 * [1. Download from Google Earth Engine](./Data_Library_Downloads)

  

 * [2. U-net model](./u-net-model)
   * [2.1. Configs](./configs)
   * [2.2. Datasets](./datasets) 
   * [2.3. Losses](./losses) 
   * [2.4. Models](./models)
   * [2.5. Model prediction](./prediction) 
   * [2.6. Visualization](./vis) 

 * [3. Evaluation](./evaluation)
   * [3.1. Case study of Shenzhen](./shenzhen_case_study)
   
* [4. Test data and models for download](https://www.research-collection.ethz.ch/handle/20.500.11850/611672)
   



