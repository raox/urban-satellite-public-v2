import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.nn.functional import one_hot
from torch import Tensor
from typing import Union

def mask_l1_loss(pred, target, mask):
    #mask = mask.unsqueeze(1).expand_as(pred).float()
    loss = F.smooth_l1_loss(pred * mask, target * mask, reduction="sum")
    loss = loss / (mask.sum() + 1e-4)
    return loss

# adapted from https://wandb.ai/capecape/classification-techniques/reports/Classification-Loss-Functions-Comparing-SoftMax-Cross-Entropy-and-More--VmlldzoxODEwNTM5
class FocalLoss(nn.Module):
    "Focal loss implemented using F.cross_entropy"
    def __init__(self, gamma: float = 2.0, weight=None, reduction: str = 'mean') -> None:
        super().__init__()
        self.gamma = gamma
        self.weight = weight
        self.reduction = reduction


    def forward(self, inp: torch.Tensor, targ: torch.Tensor):
        ce_loss = F.cross_entropy(inp, targ, weight=self.weight, reduction="none")
        p_t = torch.exp(-ce_loss)
        loss = (1 - p_t)**self.gamma * ce_loss
        if self.reduction == "mean":
            loss = loss.mean()
        elif self.reduction == "sum":
            loss = loss.sum()
        return loss


class MultitaskLoss(nn.Module):
    def __init__(self):
        super(MultitaskLoss, self).__init__()
        self.cls_loss_fn = FocalLoss(gamma = 2.0)

    def forward(self, pred, target):
        #cls_loss = F.cross_entropy(pred["cls"], target["cls"], reduction="mean")
        cls_loss = self.cls_loss_fn(pred["cls"], target["cls"])
        height_loss = mask_l1_loss(pred["height"], target["height"], target["cls"])
        
        loss = cls_loss + height_loss
        return {"loss": loss, "cls": cls_loss.item(), "height": height_loss.item()} 