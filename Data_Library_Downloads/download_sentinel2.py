import os
import folium
import ee
from datetime import datetime
import time
from tqdm import tqdm
import logging
import pandas as pd
import argparse

YEARS = [2018, 2019, 2020, 2021, 2022]

# B4 is red, B3 is green, B2 is blue, B8 is NIR
# https://developers.google.com/earth-engine/datasets/catalog/COPERNICUS_S2_SR#bands
CLD_MASK_NAME = 'cloudmask'
BANDS = ['B4', 'B3', 'B2', 'B8', CLD_MASK_NAME]

GEO_EXTENT_THRESH = 6500000000  # square meters
EXPORT2DRIVE = True
DRIVE_FOLDER = "Sentinel2_"
CLOUD_FILTER = 60
CLD_PRB_THRESH = 80
NIR_DRK_THRESH = 0.15
CLD_PRJ_DIST = 2
BUFFER = 100
tasklist = []
CITY_CSV = r"bound_box_L3.csv"
LOG_FILE = r"LogInfo/Sentinel2_downloads.log"
os.makedirs("LogInfo", exist_ok=True)


def getSentinalS2SRImage(aoi, *years, cname, export=True,):
    # define the area of interest, using the Earth Engines geometry object
    city_images = []

    for year in years[0]:
        logging.info("City %s -- Year %s", cname, year)
        start_date = str(year) + '-01-01'
        end_date = str(year) + '-12-31'

        s2_sr_cld_col = get_s2_sr_cld_col(aoi, start_date, end_date)

        if not s2_sr_cld_col:
            logging.info("     No image in {}".format(start_date[:4]))
            #city_images.append(year)
            continue

        # apply the mask and reduce the collection using median value
        sr_apply_mask = s2_sr_cld_col.map(add_cld_shdw_mask).map(apply_cld_shdw_mask)
        # sr_apply_mask = s2_sr_cld_col.map(add_cld_shdw_mask)

        s2_sr_median = (sr_apply_mask.median())
        # s2_sr_median = sr_apply_mask.mosaic()
        city_img = s2_sr_median.select(BANDS)
        city_img = city_img.set({"year": year})
        city_images.append(city_img)
        logging.info("     Add the image with cloud mask to image collection.")

    if len(city_images) and export:
        city_imgcol = ee.ImageCollection.fromImages(city_images)
        export2GDrive(city_imgcol, cname, aoi)
        time.sleep(10)


def export2GDrive(img_collection, city_name, aoi):
    tasklist = []
    img_list = img_collection.toList(img_collection.size())
    n = 0
    while True:
        try:
            img = ee.Image(img_list.get(n))
            city_year = img.getInfo()["properties"]["year"]
            description = city_name + str(city_year)
            task = ee.batch.Export.image.toDrive(image=img,
                                                 description=description,
                                                 folder=DRIVE_FOLDER + city_name,
                                                 fileNamePrefix=description,
                                                 region=aoi,
                                                 scale=10)
            task.start()
            logging.info("exporting {} to folder '{}' in GDrive".format(description, DRIVE_FOLDER))

            tasklist.append(task)
            n += 1
        except Exception as e:
            error = str(e).split(':')
            logging.info(f"{e}")
            if error[0] == 'List.get':
                break

    return tasklist


def get_s2_sr_cld_col(aoi, start_date, end_date):
    # Import and filter S2 SR.
    s2_sr_col = (ee.ImageCollection('COPERNICUS/S2_SR')
                 .filterBounds(aoi)
                 .filterDate(start_date, end_date)
                 .filter(ee.Filter.lte('CLOUDY_PIXEL_PERCENTAGE', CLOUD_FILTER)))
    logging.info("     Find {} images of Sentinel2 MSI.".format(s2_sr_col.size().getInfo()))
    # .filter(ee.Filter.rangeContains('CLOUDY_PIXEL_PERCENTAGE', 45, 60)))

    # Import and filter s2cloudless.
    s2_cloudless_col = (ee.ImageCollection('COPERNICUS/S2_CLOUD_PROBABILITY')
                        .filterBounds(aoi)
                        .filterDate(start_date, end_date))
    logging.info("     Find {} images of Sentinel2 Cloud Probability.".format(s2_cloudless_col.size().getInfo()))

    if s2_sr_col.size().getInfo():
        # Join the filtered s2cloudless collection to the SR collection by the 'system:index' property.
        return ee.ImageCollection(ee.Join.saveFirst('s2cloudless').apply(**{
            'primary': s2_sr_col,
            'secondary': s2_cloudless_col,
            'condition': ee.Filter.equals(**{
                'leftField': 'system:index',
                'rightField': 'system:index'
            })
        }))
    else:
        return None


def add_cloud_bands(img):
    # Get s2cloudless image, subset the probability band.
    cld_prb = ee.Image(img.get('s2cloudless')).select('probability')

    # Condition s2cloudless by the probability threshold value.
    is_cloud = cld_prb.gt(CLD_PRB_THRESH).rename('clouds')

    # Add the cloud probability layer and cloud mask as image bands.
    return img.addBands(ee.Image([cld_prb, is_cloud]))


def add_shadow_bands(img):
    # Identify water pixels from the SCL band.
    not_water = img.select('SCL').neq(6)

    # Identify dark NIR pixels that are not water (potential cloud shadow pixels).
    SR_BAND_SCALE = 1e4
    dark_pixels = img.select('B8').lt(NIR_DRK_THRESH * SR_BAND_SCALE).multiply(not_water).rename('dark_pixels')

    # Determine the direction to project cloud shadow from clouds (assumes UTM projection).
    shadow_azimuth = ee.Number(90).subtract(ee.Number(img.get('MEAN_SOLAR_AZIMUTH_ANGLE')));

    # Project shadows from clouds for the distance specified by the CLD_PRJ_DIST input.
    cld_proj = (img.select('clouds').directionalDistanceTransform(shadow_azimuth, CLD_PRJ_DIST * 10)
                .reproject(**{'crs': img.select(0).projection(), 'scale': 100})
                .select('distance')
                .mask()
                .rename('cloud_transform'))

    # Identify the intersection of dark pixels with cloud shadow projection.
    shadows = cld_proj.multiply(dark_pixels).rename('shadows')

    # Add dark pixels, cloud projection, and identified shadows as image bands.
    return img.addBands(ee.Image([dark_pixels, cld_proj, shadows]))


def add_cld_shdw_mask(img):
    # Add cloud component bands.
    img_cloud = add_cloud_bands(img)

    # Add cloud shadow component bands.
    img_cloud_shadow = add_shadow_bands(img_cloud)

    # Combine cloud and shadow mask, set cloud and shadow as value 1, else 0.
    is_cld_shdw = img_cloud_shadow.select('clouds').add(img_cloud_shadow.select('shadows')).gt(0)

    # Remove small cloud-shadow patches and dilate remaining pixels by BUFFER input.
    # 20 m scale is for speed, and assumes clouds don't require 10 m precision.
    is_cld_shdw = (is_cld_shdw.focalMin(2).focalMax(BUFFER * 2 / 20)
                   .reproject(**{'crs': img.select([0]).projection(), 'scale': 20})
                   .rename(CLD_MASK_NAME))

    # Add the final cloud-shadow mask to the image.
    return img_cloud_shadow.addBands(is_cld_shdw)


# Define a method for displaying Earth Engine image tiles to a folium map.
def add_ee_layer(self, ee_image_object, vis_params, name, show=True, opacity=1, min_zoom=0):
    map_id_dict = ee.Image(ee_image_object).getMapId(vis_params)
    folium.raster_layers.TileLayer(
        tiles=map_id_dict['tile_fetcher'].url_format,
        attr='Map Data &copy; <a href="https://earthengine.google.com/">Google Earth Engine</a>',
        name=name,
        show=show,
        opacity=opacity,
        min_zoom=min_zoom,
        overlay=True,
        control=True
    ).add_to(self)


def display_cloud_layers(col, AOI, year):
    # Mosaic the image collection.
    img = col.mosaic()

    # Subset layers and prepare them for display.
    clouds = img.select('clouds').selfMask()
    shadows = img.select('shadows').selfMask()
    dark_pixels = img.select('dark_pixels').selfMask()
    probability = img.select('probability')
    cloudmask = img.select(CLD_MASK_NAME).selfMask()
    cloud_transform = img.select('cloud_transform')

    # Create a folium map object.
    center = AOI.centroid(10).coordinates().reverse().getInfo()
    m = folium.Map(location=center, zoom_start=12)

    # Add layers to the folium map.
    m.add_ee_layer(img,
                   {'bands': ['B4', 'B3', 'B2'], 'min': 0, 'max': 2500, 'gamma': 1.1},
                   'S2 image', True, 1, 9)
    m.add_ee_layer(probability,
                   {'min': 0, 'max': 100},
                   'probability (cloud)', False, 1, 9)
    m.add_ee_layer(clouds,
                   {'palette': 'e056fd'},
                   'clouds', False, 1, 9)
    m.add_ee_layer(cloud_transform,
                   {'min': 0, 'max': 1, 'palette': ['white', 'black']},
                   'cloud_transform', False, 1, 9)
    m.add_ee_layer(dark_pixels,
                   {'palette': 'orange'},
                   'dark_pixels', False, 1, 9)
    m.add_ee_layer(shadows, {'palette': 'yellow'},
                   'shadows', False, 1, 9)
    m.add_ee_layer(cloudmask, {'palette': 'orange'},
                   'cloudmask', True, 0.5, 9)

    # Add a layer control panel to the map.
    m.add_child(folium.LayerControl())

    # Display the map.
    display_dir = "//display"
    if not os.path.isdir(display_dir):
        os.mkdir(display_dir)
    m.save(display_dir + '//cloud_test_{}.html'.format(year))


def apply_cld_shdw_mask(img):
    # Subset the cloudmask band and invert it so clouds/shadow are 0, else 1.
    not_cld_shdw = img.select(CLD_MASK_NAME).Not()

    # Subset reflectance bands and update their masks, return the result.
    return img.updateMask(not_cld_shdw)


def form_coords(min_lon, min_lat, max_lon, max_lat):
    coords = [
        [min_lon, min_lat],
        [max_lon, min_lat],
        [max_lon, max_lat],
        [min_lon, max_lat],
        [min_lon, min_lat]
    ]
    return coords


def crop_coords(coords):
    new_coords = []

    min_lon = coords[0][0]
    min_lat = coords[0][1]
    max_lon = coords[1][0]
    max_lat = coords[2][1]

    lon_span = max_lon - min_lon
    lat_span = max_lat - min_lat

    crop1 = form_coords(min_lon, min_lat, min_lon + lon_span / 2, min_lat + lat_span / 2)
    new_coords.append(crop1)

    crop2 = form_coords(min_lon, min_lat + lat_span / 2, min_lon + lon_span / 2, max_lat)
    new_coords.append(crop2)

    crop3 = form_coords(min_lon + lon_span / 2, min_lat, max_lon, min_lat + lat_span / 2)
    new_coords.append(crop3)

    crop4 = form_coords(min_lon + lon_span / 2, min_lat + lat_span / 2, max_lon, max_lat)
    new_coords.append(crop4)

    return new_coords


def process(coords, *years, cname, export=True):
    aoi = ee.Geometry.Polygon(coords)
    aoi_area = aoi.area().getInfo()
    if aoi_area > GEO_EXTENT_THRESH:
        new_split_coords = crop_coords(coords)

        for i in range(len(new_split_coords)):
            city_name = cname + "split" + str(i) + "_"
            print(city_name)
            new_coords = new_split_coords[i]
            process(new_coords, years[0], cname=city_name, export=export)
    else:
        getSentinalS2SRImage(aoi, years[0], cname=cname, export=export)


if __name__ == "__main__":
    logging.basicConfig(filename=LOG_FILE, filemode='w', level=logging.DEBUG,
                        format='%(asctime)s - %(levelname)s - %(message)s')
    # Note: The authentication with gcloud CLI is needed. After the gcloud is installed, the restart of PC may be need.
    ee.Authenticate()
    ee.Initialize()

    # with open(r"key_path_ethAccount/service_account.txt") as f:
    #     service_account = f.read().replace('\n', '')
    # credentials = ee.ServiceAccountCredentials(service_account, r"key_path_ethAccount/.private-key.json")
    # ee.Initialize(credentials)

    start_time = datetime.now()

    cities = pd.read_csv(CITY_CSV)

    for i in tqdm(range(len(cities))):
        if type(cities.iloc[i]["city"]) != str:
            city_name = str(cities.iloc[i]["city"]) + "_"
        else:
            city_name = cities.iloc[i]["city"]

        min_lon = cities.iloc[i]["min_lon"]
        max_lon = cities.iloc[i]["max_lon"]
        min_lat = cities.iloc[i]["min_lat"]
        max_lat = cities.iloc[i]["max_lat"]

        coords = [
            [min_lon, min_lat],
            [max_lon, min_lat],
            [max_lon, max_lat],
            [min_lon, max_lat],
            [min_lon, min_lat]
        ]

        process(coords, YEARS, cname=city_name, export=EXPORT2DRIVE)

    end_time = datetime.now()
    logging.info("Script Run Time - Start: %s", start_time.strftime("%Y-%m-%d %H:%M:%S"))
    logging.info("Script Run Time -   End: %s", end_time.strftime("%Y-%m-%d %H:%M:%S"))
