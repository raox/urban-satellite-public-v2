# Downloading via Google Earth Engine

## Set Up

### Google Cloud
Make sure [gcloud](https://cloud.google.com/sdk/docs/install) is installed in your device. Follow the guidance based on your os. Don't forget to initilize gcloud.

### Python Environments
```
conda create -n  data_pre
conda activate data_pre
conda install -c conda-forge earthengine-api    # alternatively using pip install
pip install tqdm
pip install pandas
```
### Earth Engine
Sign up for earth engine: https://courses.spatialthoughts.com/gee-sign-up.html. Go to non-commertial use option1.

### Google Drive Credentials 
This is needed to run the gdrive2local.py script.
1. Go to the [Google Developers Console](https://console.cloud.google.com/cloud-resource-manager)
2. Create a New Project (“New Project” button).
3. Enable the Google Drive API:
    - In the Dashboard, click on “Enable APIs and Services”.
    - In the API Library, search for “Google Drive API”.
    - Click on the Google Drive API and then click “Enable”.
4. Create Credentials:
    - Go to the “Credentials” tab on the left side.
    - Click on “Create Credentials” at the top of the page.
    - Choose “OAuth client ID”.
    - You may be prompted to configure the consent screen; if so, click on “Configure consent screen”.
        - Select “External” and click “Create”.
        - Fill in the required fields under “App information” (like App name, User support email, and Developer contact information).
        - Click “Save and Continue”.
        - You don’t need to add any scopes on the “Scopes” page, so just click “Save and Continue”.
        - On the “Test users” page, add your email address as a test user, then click “Save and Continue”.
        - Back in the “Credentials” tab, click “Create Credentials” again and choose “OAuth client ID”.
5. Configure OAuth Consent Screen:
    - Application type: Choose “Desktop app”.
    - Name your OAuth 2.0 client and click “Create”.
6. Download the JSON File:
    - After creating the client ID, you will see a confirmation screen. Click “OK”.
    - Next to the client ID you just created, click the download icon to download the JSON file.
    - Rename this file to **`credentials.json`** and place it in the same directory as your Python script.
7. Then install the following python environments:
    ```
    pip install --upgrade google-api-python-client google-auth-httplib2 google-auth-oauthlib
    ```

## Running Downloading Pipeline
**Sentinel2**

- download_sentinel2.py: the script implements selecting data, applying filters and sending queries from the clien-side. The logging output is saved in `LogInfo` folder.

**Sentinel1**

- download_sentinel1.py: the script implements selecting data, applying filters and sending queries from the clien-side. The logging output is saved in `LogInfo` folder.


## Parameters

### Sentinel2
- **Source**: [Sentinel-2 MSI, Level-2A](https://developers.google.com/earth-engine/datasets/catalog/COPERNICUS_S2_SR)
- **Filter parameters**
    - years coverage:  2017 -- 2022
    - bands: `B4`(red), `B3`(green), `B2`(blue), `B8`(NIR)
    - determine region extent: Each city has a specified bounding box, stored in a csv file. When the area of the extent exceeds the `GEO_EXTENT_THRESH` (6500000000 square meters), the extent division strategy is used.
    - cloud coverage: The images with `CLOUDY_PIXEL_PERCENTAGE` over 60 are filtered out.
    - band values: (todo) currently no band values processing.
- **Cloud removing**
    - Cloud detection: Basically follows the [tutorial] https://developers.google.com/earth-engine/tutorials/community/sentinel-2-s2cloudless).
    - Parameters:  Currently used default values in the tutorial above.

### Sentinel1 
- **Source**: [Sentine-l SAR GRD](https://developers.google.com/earth-engine/datasets/catalog/COPERNICUS_S1_GRD)
- **Filter parameters**
    - years coverage:  2015 -- 2022
    - bands: `VV` & `VH`
    - instrumentMode: `IW`