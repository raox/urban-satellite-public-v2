import logging
import os
import ee
from datetime import datetime
import time
from tqdm import tqdm
import pandas as pd

YEARS = [2015, 2016, 2017, 2018, 2019, 2020, 2021, 2022]
GEO_EXTENT_THRESH = 6500000000  # square meters
EXPORT2DRIVE = True
DRIVE_FOLDER = "Sentinel1_"

CITY_CSV = r"bound_box_L3.csv"
LOG_FILE = r"LogInfo/Sentinel1_downloads.log"
os.makedirs("LogInfo", exist_ok=True)


def getSentinalS1Image(aoi, *years, cname, export=True,):
    city_imgs= []
    orbit = "ASCENDING"
    for year in years[0]:
        logging.info("City %s -- Year %s", cname, year)
        start_date = str(year) + '-12-01'
        end_date = str(year) + '-12-31'
        s1_col = (ee.ImageCollection('COPERNICUS/S1_GRD')
                  .filterBounds(aoi)
                  .filterDate(start_date, end_date)
                  # .filter(ee.Filter.eq('orbitProperties_pass', orbit))
                  .filter(ee.Filter.listContains('transmitterReceiverPolarisation', 'VV'))
                  .filter(ee.Filter.listContains('transmitterReceiverPolarisation', 'VH'))
                  .filter(ee.Filter.eq('instrumentMode', 'IW'))
                  .select(["VV", "VH"])
                  )

        logging.info("Find {} images of Sentinel1 SAR for year {} ".format(s1_col.size().getInfo(), year))
        city_img = ee.Image.cat(s1_col.mean())

        city_img = city_img.set({"year": year})
        city_imgs.append(city_img)

    if len(city_imgs) and export:
        city_imgcol = ee.ImageCollection.fromImages(city_imgs)
        export2GDrive(city_imgcol, cname,aoi)
        time.sleep(10)


def export2GDrive(img_collection, city_name, aoi):
    # empty tasks list
    tasklist = []
    img_list = img_collection.toList(img_collection.size())

    n = 0
    while True:
        try:
            img = ee.Image(img_list.get(n))
            city_year = img.getInfo()["properties"]["year"]
            description = city_name + str(city_year)
            task = ee.batch.Export.image.toDrive(image=img,
                                                 description=description,
                                                 folder=DRIVE_FOLDER + city_name,
                                                 fileNamePrefix=description,
                                                 region=aoi,
                                                 scale=10
                                                 )
            task.start()
            print("exporting {} to folder '{}' in GDrive".format(description, DRIVE_FOLDER))

            tasklist.append(task)
            n += 1
        except Exception as e:
            error = str(e).split(':')
            logging.info(f"{e}")
            if error[0] == 'List.get':
                break
            else:
                raise e

    return tasklist

def form_coords(min_lon, min_lat, max_lon, max_lat):
    coords = [
        [min_lon, min_lat],
        [max_lon, min_lat],
        [max_lon, max_lat],
        [min_lon, max_lat],
        [min_lon, min_lat]
    ]
    return coords


def crop_coords(coords):
    new_coords = []

    min_lon = coords[0][0]
    min_lat = coords[0][1]
    max_lon = coords[1][0]
    max_lat = coords[2][1]

    lon_span = max_lon - min_lon
    lat_span = max_lat - min_lat

    crop1 = form_coords(min_lon, min_lat, min_lon + lon_span / 2, min_lat + lat_span / 2)
    new_coords.append(crop1)

    crop2 = form_coords(min_lon, min_lat + lat_span / 2, min_lon + lon_span / 2, max_lat)
    new_coords.append(crop2)

    crop3 = form_coords(min_lon + lon_span / 2, min_lat, max_lon, min_lat + lat_span / 2)
    new_coords.append(crop3)

    crop4 = form_coords(min_lon + lon_span / 2, min_lat + lat_span / 2, max_lon, max_lat)
    new_coords.append(crop4)

    return new_coords


def process(coords, *years, cname, export=True):
    aoi = ee.Geometry.Polygon(coords)
    aoi_area = aoi.area().getInfo()
    if aoi_area > GEO_EXTENT_THRESH:
        new_split_coords = crop_coords(coords)

        for i in range(len(new_split_coords)):
            city_name = cname + "split" + str(i) + "_"
            print(city_name)
            new_coords = new_split_coords[i]
            process(new_coords, years[0], cname=city_name, export=export)
    else:
        getSentinalS1Image(aoi, years[0], cname=cname, export=export)


if __name__ == "__main__":
    logging.basicConfig(filename=LOG_FILE, filemode='w', level=logging.DEBUG,
                        format='%(asctime)s - %(levelname)s - %(message)s')
    # Note: The authentication with gcloud CLI is needed. After the gcloud is installed, the restart of PC may be need.
    # Trigger the authentication flow.
    ee.Authenticate()
    # Initialize the library.
    ee.Initialize()

    # with open(r"key_path_ethAccount/service_account.txt") as f:
    #     service_account = f.read().replace('\n', '')
    # credentials = ee.ServiceAccountCredentials(service_account, r"key_path_ethAccount/.private-key.json")
    # ee.Initialize(credentials)

    start_time = datetime.now()

    cities = pd.read_csv(CITY_CSV)

    for i in tqdm(range(len(cities))):

        if type(cities.iloc[i]["city"]) != str:
            city_name = str(cities.iloc[i]["city"]) + "_"
        else:
            city_name = cities.iloc[i]["city"]
        min_lon = cities.iloc[i]["min_lon"]
        max_lon = cities.iloc[i]["max_lon"]
        min_lat = cities.iloc[i]["min_lat"]
        max_lat = cities.iloc[i]["max_lat"]

        coords = [
            [min_lon, min_lat],
            [max_lon, min_lat],
            [max_lon, max_lat],
            [min_lon, max_lat],
            [min_lon, min_lat]
        ]

        process(coords, YEARS, cname=city_name, export=EXPORT2DRIVE)
        

    end_time = datetime.now()
    print("Script Run Time - Start: ", start_time.strftime("%Y-%m-%d %H:%M:%S"))
    print("Script Run Time -   End: ", end_time.strftime("%Y-%m-%d %H:%M:%S"))
