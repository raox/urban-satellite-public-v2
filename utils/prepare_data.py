import os
import numpy as np
import tifffile
import cv2
import math
import glob


def load_RS_building_s3(sentinel1_path, sentinel2_path, building_path, city): 
    sentinel1_file = city + "2017.tif"
    sentinel2_file = None
    
    sentinel1_image = os.path.join(sentinel1_path, "Sentinel1_" + city, sentinel1_file)

    for year in [2017, 2018, 2019]:
        path = os.path.join(sentinel2_path, "Sentinel2_" + city, city + str(year) + ".tif")
        if os.path.isfile(path):
            sentinel2_file = city + str(year) + ".tif"
            sentinel2_image = os.path.join(sentinel2_path, "Sentinel2_" + city, sentinel2_file)
            break

    if sentinel2_file is None:
        return None, None    
    
    print("Chosen Sentinel2 file: ", sentinel2_file)
    bd_file = city + '_height_raster.tif'
    bd_image = os.path.join(building_path, bd_file)
    #bd_img = np.asarray(cv2.imread(bd_image, cv2.IMREAD_GRAYSCALE))
    bd_img = np.asarray(tifffile.imread(bd_image))
    
    sentinel1_tif = np.asarray(tifffile.imread(sentinel1_image))
    sentinel2_tif = np.asarray(tifffile.imread(sentinel2_image))
    #sentinel2_tif = np.nan_to_num(sentinel2_tif, nan=-1)
    
    combo_tif = np.dstack((sentinel1_tif,sentinel2_tif))

    return combo_tif, bd_img    


#subseting and saving sub-images
# the size of sub images is measured by the number of pixels

def subset(building_image, RS_image, savepath, size=64, cover_threshold = 0.1):
    global count

    x_limit = math.floor(np.shape(building_image)[0]/size)
    y_limit = math.floor(np.shape(building_image)[1]/size)
    saved = 0
    
    for i in range(x_limit):
        for j in range(y_limit):
            sub_bd_image = building_image[i * size: (i+1) * size, j * size: (j+1) * size]
            sub_RS_image = RS_image[i * size: (i+1) * size, j * size: (j+1) * size]
                    
            nans = np.sum(np.isnan(sub_RS_image[:, :, 2:6]), axis = 2)
            ratio = np.sum(nans > 1) / (nans.shape[0] * nans.shape[1])
            
            if ratio < 0.2:
                saved += 1
                if (np.sum(sub_bd_image > 0)/(size*size) >= cover_threshold) & (np.shape(sub_RS_image) == (64,64,7)):
                    bd_filename = str(count) + "_building.tif"
                    tifffile.imwrite(os.path.join(savepath, bd_filename), sub_bd_image)

                    RS_filename = str(count) + "_RS.tif"
                    tifffile.imwrite(os.path.join(savepath, RS_filename), sub_RS_image[:,:,0:6])

                    count += 1  

    print("nan < threshold: ", saved, "/", x_limit * y_limit)

def write_to_file(filename, data):
    with open(filename, 'a') as f:
        f.write(str(data))
        f.write('\n')


if __name__ == "__main__":
    sentinel1_path = "/data-nfs/urban-econ/Sentinel1_Downloads/"
    sentinel2_path = "/data-nfs/urban-econ/Sentinel2_Downloads/"
    building_path = "/data-nfs/urban-econ/building_height_raster_regression"
    
    savepath = "/data-nfs/sapar/data/sub"
    count = 0

    # get city names that have labels
    city_path = glob.glob(os.path.join(building_path, "*.tif"))
    ext = "_height_raster.tif"
    city_names = []
    for path in city_path:
        filename = path.split("/")[-1]
        city_name = filename.replace(ext, "")
        city_names.append(city_name)
    
    # for city in city_names:
    #     combo_tif, bd_img = load_RS_building_s3(sentinel1_path, sentinel2_path, building_path, city)

    #divide data into sub-parts if not already done
    if not os.path.exists(savepath):
        os.mkdir(savepath)
        for city in city_names:
            print(city)
            if city == "Hangzhou":
                print("Sentinel2 2017-2018-2019 does not exist")
                continue
            combo_tif, bd_img = load_RS_building_s3(sentinel1_path, sentinel2_path, building_path, city)
            if not combo_tif is None:
                subset(bd_img, combo_tif, savepath, size=64, cover_threshold = 0.1)
            else:
                print("Sentinel2 2017-2018-2019 does not exist")

    print("total {} images saved".format(str(count)))
    
    # randomly create list for train, val and test
    _, _, files = next(os.walk(savepath))
    num_data = len(files) // 2
    train_percent = 0.8
    val_percent = 0.1

    num_train = int(num_data * train_percent)
    num_val = int(num_data * val_percent)
    idxs = np.arange(num_data)
    np.random.shuffle(idxs)

    train_idxs = idxs[:num_train]
    val_idxs = idxs[num_train: num_train + num_val]
    test_idxs = idxs[num_train + num_val:]

    train_filename = "/data-nfs/sapar/data/list/train.txt"
    val_filename = "/data-nfs/sapar/data/list/val.txt"
    test_filename = "/data-nfs/sapar/data/list/test.txt"

    for idx in train_idxs:
        write_to_file(train_filename, idx)
    for idx in val_idxs:
        write_to_file(val_filename, idx)
    for idx in test_idxs:
        write_to_file(test_filename, idx)

    print("train/val/test: ", train_idxs.shape[0], val_idxs.shape[0], test_idxs.shape[0])