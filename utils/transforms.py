import numpy as np
import torch
import torchvision.transforms.functional as TF

#compose several transformations
class Compose(object):
    def __init__(self, transforms, p=1.0):
        self.transforms = transforms
        self.p = p

    def __call__(self, data):
        if np.random.random() <= self.p:
            for t in self.transforms:
                data = t(data)
        return data

# select one of the transforms and apply
class OneOf(object):
    def __init__(self, transforms, p=1.0):
        self.transforms = transforms
        self.p = p

    def __call__(self, data):
        if np.random.random() < self.p and len(self.transforms) > 0:
            choice = np.random.randint(low=0, high=len(self.transforms))
            data = self.transforms[choice](data)

        return data

# apply random rotation with angle within limit_angle and with probability p
class RandomRotation(object):
    def __init__(self, limit_angle=20.):
        self.limit_angle = limit_angle

    def __call__(self, data):
        angle = np.random.uniform(-self.limit_angle, self.limit_angle)
        aug_data = []
        for i in range(len(data)):
            aug_data.append(TF.rotate(data[i], angle))

        return aug_data

# randomly erase some pixels from satellite 1     
class RandomErase(object):
    def __init__(self, max_pixels = 30):
        self.max_pixels = max_pixels

    def __call__(self, data):
        aug_data = []
        image = data[0]
        num_pixels = np.random.randint(self.max_pixels)
        y = np.random.randint(image.shape[1], size = num_pixels)
        x = np.random.randint(image.shape[2], size = num_pixels)

        image[:2, y, x] = 0
        aug_data.append(image)

        for i in range(1, len(data)):
            aug_data.append(data[i])

        return aug_data
            


# apply affine transformation within provided limits
class Affine(object):
    def __init__(self, limit_angle, limit_translation, limit_shear, limit_scale):
        self.limit_angle = limit_angle
        self.limit_translation = limit_translation
        self.limit_shear = limit_shear
        self.limit_scale = limit_scale

    def __call__(self, data):
        aug_data = []
        angle = np.random.uniform(-self.limit_angle, self.limit_angle)
        shear_angle = np.random.uniform(-self.limit_shear, self.limit_shear)
        scale = np.random.uniform(self.limit_scale[0], self.limit_scale[1])
        translate_x = np.random.randint(self.limit_translation)
        translate_y = np.random.randint(self.limit_translation)
        translate = [translate_x, translate_y]
        for i in range(len(data)):
            aug_data.append(TF.affine(data[i], angle, translate, scale, shear = shear_angle))
        
        return aug_data