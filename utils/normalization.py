import os
import numpy as np
import tifffile
from tqdm import tqdm

def create_data_list(file):
    data_list = []
    with open(file, "r") as f:
        for line in f:
            data_list.append(line.strip())
    
    return data_list

if __name__ == "__main__":
    train_file = "/data-nfs/sapar/data/list/citywise256_train.txt"
    val_file = "/data-nfs/sapar/data/list/citywise256_val.txt"
    data_location = "/data-nfs/sapar/data/citywise256"
    
    files = create_data_list(train_file)
    files.extend(create_data_list(val_file))

    # calculate mean
    avgs = []
    for file in tqdm(files):
        image_path = os.path.join(data_location, file + "_RS.tif")
        image = np.asarray(tifffile.imread(image_path))
        
        avgs.append(np.nanmean(image, axis = (0, 1)))
        
    mean = np.mean(np.array(avgs), axis = 0)
    
    # calculate std
    x = []
    for file in tqdm(files):
        image_path = os.path.join(data_location, file + "_RS.tif")
        image = np.asarray(tifffile.imread(image_path))

        x.append(np.nanmean((image - mean)**2, axis = (0, 1)))

    std = np.sqrt(np.mean(np.array(x), axis = 0))
    print("mean: ", mean)
    print("std: ", std)
    
    