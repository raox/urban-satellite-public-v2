import torch

def mean_absolute_error(pred, target):
    return torch.mean(torch.abs(pred - target))

def seg_scores(pred, target):
    tp_idxs = torch.logical_and(pred == 1, pred == target)
    fp_idxs = torch.logical_and(pred == 1, pred != target)
    tn_idxs = torch.logical_and(pred == 0, pred == target)
    fn_idxs = torch.logical_and(pred == 0, pred != target)

    return (tp_idxs.sum(), fp_idxs.sum(), tn_idxs.sum(), fn_idxs.sum())

def iou(pred, target):
    intersection = torch.logical_and(pred, target)
    union = torch.logical_or(pred, target)

    return intersection / union

def mean_relative_error(pred, target):
    return torch.mean(torch.abs(pred - target) / target)