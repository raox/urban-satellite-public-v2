import json
import torch
from torch.utils.data import DataLoader

from datasets.reg_dataset import RegDataset
from models.unet import Unet
from models.unet_variants import UnetVariant
from utils.metrics import mean_absolute_error

from tqdm import tqdm
import numpy as np
import matplotlib.pyplot as plt


if __name__ == "__main__":
    data_file = "/home/sapar/work/data/list/citywise256_test.txt"
    data_location = "/home/sapar/work/data/citywise256"
    with open("/home/sapar/work/unet-v2/configs/base.json", 'r') as f:
        config = json.load(f)

    dataset = RegDataset(data_file, data_location, config["norm"], None)
    loader = DataLoader(dataset, shuffle=False, batch_size=1)

    model_path = "/home/sapar/experiments/remote_sensing/se_resnet50_256_sliding_400_22-04-2023_1/best_checkpoints/70epoch"
    model = UnetVariant(config["model"])
    
    model.load_state_dict(torch.load(model_path, map_location=torch.device('cpu')))
    model.eval()

    err = []
    bins = [1, 4, 7, 10]
    bin_err = []
    sum_err = []
    for bin in range(len(bins) + 1) :
        bin_err.append(0)
        sum_err.append(0)

    hist_bins = np.arange(1, 100, 3)
    first = True

    for target in tqdm(loader):
        image = target["image"].to(config["device"])
        pred = model(target["image"])
        #pred_height = torch.exp(pred["height"].detach())
        pred_height = pred["height"].detach() * 400
        pred_cls = torch.softmax(pred["cls"], dim = 1).detach()
        pred_cls = torch.argmax(pred_cls, dim = 1)
        pred_height = (pred_height * pred_cls).cpu()
        target_height = target["orig_height"]
        
        # metrics according to bins
        idxs = target_height == 0
        bin_err[0] += torch.sum((pred_height[idxs] - target_height[idxs])) 
        sum_err[0] += torch.sum(idxs)
        
        for i in range(len(bins) - 1):
            start = bins[i]
            end = bins[i + 1]
            idxs = torch.logical_and(target_height >= start, target_height < end)

            bin_err[i+1] += torch.sum((pred_height[idxs] - target_height[idxs]))
            sum_err[i+1] += torch.sum(idxs)

        idxs = target_height >= bins[-1]
        bin_err[-1] += torch.sum((pred_height[idxs] - target_height[idxs]))
        sum_err[-1] += torch.sum(idxs)

        err.append(mean_absolute_error(pred_height, target_height))

        if first:
            hist_pred = np.histogram(pred_height.squeeze().numpy(), hist_bins)[0]
            hist_target = np.histogram(target_height.squeeze().numpy(), hist_bins)[0]
            hist_errors = np.histogram(np.abs(pred_height.squeeze().numpy() - target_height.squeeze().numpy()), hist_bins)[0]
            first = False
        else:
            hist_pred += np.histogram(pred_height.squeeze().numpy(), hist_bins)[0]
            hist_target += np.histogram(target_height.squeeze().numpy(), hist_bins)[0]
            hist_errors += np.histogram(np.abs(pred_height.squeeze().numpy() - target_height.squeeze().numpy()), hist_bins)[0]

    for i in range(len(bin_err)):
        print("bin ", i, bin_err[i] / sum_err[i])
    
    print(torch.tensor(err).mean())

    fig, ax = plt.subplots(1, 2, sharey=True)
    ax[0].hist(hist_bins[:-1], hist_bins, weights=hist_pred)
    ax[0].set_title("pred")

    ax[1].hist(hist_bins[:-1], hist_bins, weights=hist_target)
    ax[1].set_title("target")
    # plt.subplot(3, 1, 1)
    # #plt.hist(hist_pred, hist_bins)
    # plt.hist(hist_bins[:-1], hist_bins, weights=hist_pred)
    # plt.title("pred")

    # plt.subplot(3, 1, 2)
    # plt.hist(hist_bins[:-1], hist_bins, weights=hist_target)
    # plt.title("target")

    # plt.subplot(3, 1, 3)
    # plt.hist(hist_bins[:-1], hist_bins, weights=hist_errors)
    # plt.title("err")
    
    # plt.tight_layout()
    plt.show()



