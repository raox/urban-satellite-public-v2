import os
import numpy as np
import tifffile
import cv2
import math
import glob


def load_RS_building_s3(sentinel1_path, sentinel2_path, building_path, city): 
    sentinel1_file = city + "2017.tif"
    sentinel2_file = None
    
    sentinel1_image = os.path.join(sentinel1_path, "Sentinel1_" + city, sentinel1_file)

    for year in [2017, 2018, 2019]:
        path = os.path.join(sentinel2_path, "Sentinel2_" + city, city + str(year) + ".tif")
        if os.path.isfile(path):
            sentinel2_file = city + str(year) + ".tif"
            sentinel2_image = os.path.join(sentinel2_path, "Sentinel2_" + city, sentinel2_file)
            break

    if sentinel2_file is None:
        return None, None    
    
    print("Chosen Sentinel2 file: ", sentinel2_file)
    bd_file = city + '_height_raster.tif'
    bd_image = os.path.join(building_path, bd_file)
    #bd_img = np.asarray(cv2.imread(bd_image, cv2.IMREAD_GRAYSCALE))
    bd_img = np.asarray(tifffile.imread(bd_image))
    
    sentinel1_tif = np.asarray(tifffile.imread(sentinel1_image))
    sentinel2_tif = np.asarray(tifffile.imread(sentinel2_image))
    #sentinel2_tif = np.nan_to_num(sentinel2_tif, nan=-1)
    
    combo_tif = np.dstack((sentinel1_tif,sentinel2_tif))

    return combo_tif, bd_img    


#subseting and saving sub-images
# the size of sub images is measured by the number of pixels

def subset(building_image, RS_image, savepath, city, size=64, cover_threshold = 0.1):
    count = 0

    x_limit = math.floor(np.shape(building_image)[0]/size)
    y_limit = math.floor(np.shape(building_image)[1]/size)
    saved = 0

    save_folder = os.path.join(savepath, city, "grid")
    if not os.path.exists(save_folder):
        os.makedirs(save_folder)
    
    for i in range(x_limit):
        for j in range(y_limit):
            sub_bd_image = building_image[i * size: (i+1) * size, j * size: (j+1) * size]
            sub_RS_image = RS_image[i * size: (i+1) * size, j * size: (j+1) * size]
                    
            nans = np.sum(np.isnan(sub_RS_image[:, :, 2:6]), axis = 2)
            ratio = np.sum(nans > 1) / (nans.shape[0] * nans.shape[1])
            
            if ratio < 0.2:
                saved += 1
                if (np.sum(sub_bd_image > 0)/(size * size) >= cover_threshold):
                    bd_filename = str(count) + "_building.tif"
                    tifffile.imwrite(os.path.join(save_folder, bd_filename), sub_bd_image)

                    RS_filename = str(count) + "_RS.tif"
                    tifffile.imwrite(os.path.join(save_folder, RS_filename), sub_RS_image[:,:,0:6])

                    count += 1  

    print("nan < threshold: ", saved, "/", x_limit * y_limit)
    print("saved: ", count, "/", x_limit * y_limit)

def sliding_subset(building_image, RS_image, savepath, city, size=64, cover_threshold = 0.1):
    count = 0

    x_limit = math.floor(np.shape(building_image)[0]/size)
    y_limit = math.floor(np.shape(building_image)[1]/size)
    x_idxs = np.arange(0, x_limit - 0.5, 0.5)
    y_idxs = np.arange(0, y_limit - 0.5, 0.5)
    saved = 0
   

    save_folder = os.path.join(savepath, city, "sliding")
    if not os.path.exists(save_folder):
        os.makedirs(save_folder)

    for i in x_idxs:
        for j in y_idxs:
            sub_bd_image = building_image[int(i * size): int((i+1) * size), int(j * size): int((j+1) * size)]
            sub_RS_image = RS_image[int(i * size): int((i+1) * size), int(j * size): int((j+1) * size)]
            
            nans = np.sum(np.isnan(sub_RS_image[:, :, 2:6]), axis = 2)
            ratio = np.sum(nans > 1) / (nans.shape[0] * nans.shape[1])
            
            if ratio < 0.2:
                saved += 1
                if (np.sum(sub_bd_image > 0) / (size * size) >= cover_threshold):
                    bd_filename = str(count) + "_building.tif"
                    tifffile.imwrite(os.path.join(save_folder, bd_filename), sub_bd_image)

                    RS_filename = str(count) + "_RS.tif"
                    tifffile.imwrite(os.path.join(save_folder, RS_filename), sub_RS_image[:,:,0:6])

                    count += 1  

    print("nan < threshold: ", saved, "/", x_idxs.shape[0] * y_idxs.shape[0])
    print("saved: ", count, "/", x_idxs.shape[0] * y_idxs.shape[0])

def write_to_file(filename, data):
    with open(filename, 'a') as f:
        f.write(str(data))
        f.write('\n')


if __name__ == "__main__":
    sentinel1_path = "/data-nfs/urban-econ/Sentinel1_Downloads/"
    sentinel2_path = "/data-nfs/urban-econ/Sentinel2_Downloads/"
    building_path = "/data-nfs/urban-econ/building_height_raster_regression"
    
    img_size = 64
    savepath = os.path.join("/data-nfs/sapar/data/", "citywise" + str(img_size)) 
    if not os.path.exists(savepath):
        os.mkdir(savepath)

    # get city names that have labels
    city_path = glob.glob(os.path.join(building_path, "*.tif"))
    ext = "_height_raster.tif"
    city_names = []
    for path in city_path:
        filename = path.split("/")[-1]
        city_name = filename.replace(ext, "")
        city_names.append(city_name)
    

    #divide data into sub-parts
    for city in city_names:
        print(city)
        if city == "Hangzhou":
            print("Sentinel2 2017-2018-2019 does not exist")
            continue
        combo_tif, bd_img = load_RS_building_s3(sentinel1_path, sentinel2_path, building_path, city)
        if not combo_tif is None:
            sliding_subset(bd_img, combo_tif, savepath, city, size=img_size, cover_threshold = 0.1)
            subset(bd_img, combo_tif, savepath, city, size=img_size, cover_threshold = 0.1)
        else:
            print("Sentinel2 2017-2018-2019 does not exist")

    
    