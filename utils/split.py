import os
import glob
import random

def get_files(location, city, folder):
    files = []
    folder_path = os.path.join(location, city, folder)
    for file in glob.glob(os.path.join(folder_path, "*RS.tif")):
        filename = file.split("/")[-1]
        index = filename.split("_")[0]
        files.append(os.path.join(city, folder, index))

    return files
    

def create_split(location, val_cities, test_cities):
    train = []
    val = []
    test = []

    city_names = os.listdir(location)
    for city in city_names:
        if city in val_cities:
            val.extend(get_files(location, city, "grid"))
        elif city in test_cities:
            test.extend(get_files(location, city, "grid"))
        else:
            train.extend(get_files(location, city, "grid"))

    return train, val, test

def save_txt(files, save_location):
    with open(save_location, 'a') as f:
        for file in files:
            f.write(file)
            f.write("\n")



if __name__ == "__main__":
    #val_cities = ["Yangzhou"]
    val_cities = []
    test_cities = ["Shenzhen"]
    location = "/data-nfs/sapar/data/citywise256"

    train_save_location = "/data-nfs/sapar/data/list/citywise256_grid_train.txt"
    val_save_location = "/data-nfs/sapar/data/list/citywise256_grid_val.txt"
    test_save_location = "/data-nfs/sapar/data/list/citywise64_test.txt"

    train, val, test = create_split(location, val_cities, test_cities)

    if len(val_cities) == 0:
        random.shuffle(train)
        split_train = train[0 : int(0.9 * len(train))]
        split_val = train[int(0.9 * len(train)):]

    train = split_train
    val = split_val
    print("train/val/test: ", len(train), len(val), len(test))
    
    save_txt(train, train_save_location)
    save_txt(val, val_save_location)
    # save_txt(test, test_save_location)