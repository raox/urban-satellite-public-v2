import numpy as np
import json
import matplotlib as mpl
import matplotlib.cm as cm

import torch
import torchvision.transforms as T
from torch.utils.data import DataLoader

import vispy
from vispy.scene import SceneCanvas
from vispy import app

from datasets.reg_dataset import RegDataset



class Vis():
    def __init__(self, dataset):
        self.index = 0
        self.dataset = dataset

        self.canvas = SceneCanvas(keys='interactive',
                                show=True,
                                size=(192, 64))
        self.canvas.events.key_press.connect(self._key_press)
        self.canvas.events.draw.connect(self._draw)
        self.canvas.show()

        self.view = self.canvas.central_widget.add_view()
        self.image = vispy.scene.visuals.Image(parent=self.view.scene)
        
        self.update_image()
   


    def update_image(self):
        data = self.dataset[self.index]
        image = data["image"][2:5, :, :].permute(1, 2, 0).numpy()
        height = data["orig_height"].squeeze().numpy()
        cls = data["cls"].numpy()
        
        frame = scale(image)
        #height_img = np.repeat(np.expand_dims(scale(height.numpy()), axis = 2), 3, axis = 2)
        height_img = self.get_colored_img(height)
        cls_img = np.zeros(frame.shape)
        cls_img[:, :, 0] = cls
        
        img = np.concatenate((frame, cls_img, height_img), 1)       

           
        self.canvas.title = str(self.index)
        self.image.set_data(img)
        self.view.camera = vispy.scene.PanZoomCamera(aspect=1)
        self.view.camera.set_range()
        self.view.camera.flip = (0, 1, 0)
        self.canvas.update()


    def get_colored_img(self, height):
        mask = height != 0
        disp_map =  height
        # vmax = np.percentile(disp_map[mask], 95)
        # vmin = np.percentile(disp_map[mask], 5)
        vmax = 30
        vmin = 3
        normalizer = mpl.colors.Normalize(vmin=vmin, vmax=vmax)
        mapper = cm.ScalarMappable(norm=normalizer, cmap='Wistia')
        mask = np.repeat(np.expand_dims(mask,-1), 3, -1)
        colormapped_im = (mapper.to_rgba(disp_map)[:, :, :3] * 255).astype(np.uint8)
        colormapped_im[~mask] = 0
        return colormapped_im / 255


    def _key_press(self, event):
        if event.key == 'Right':
            if self.index < len(self.dataset) - 1:
                self.index += 1
            self.update_image()

        if event.key == 'Left':
            if self.index > 0:
                self.index -= 1
            self.update_image()

        if event.key == 'Q':
            self.destroy()

    def destroy(self):
        # destroy the visualization
        self.canvas.close()
        vispy.app.quit()

    def _draw(self, event):
        if self.canvas.events.key_press.blocked():
            self.canvas.events.key_press.unblock()

    def run(self):
        self.canvas.app.run()

def scale(matrix):
    arr = matrix
    new_arr = ((arr - arr.min()) * (1/(arr.max() - arr.min())))
    return new_arr



if __name__ == "__main__":
    data_file = "/home/sapar/work/test.txt"
    data_location = "/home/sapar/work/data/sub"
    with open("/home/sapar/work/unet-v2/configs/base.json", 'r') as f:
        config = json.load(f)

    dataset = RegDataset(data_file, data_location, config["norm"], config["augmentation"])
    
    vis = Vis(dataset)
    vis.run()