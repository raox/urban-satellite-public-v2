import numpy as np
import json
import matplotlib as mpl
import matplotlib.cm as cm

import torch
import torchvision.transforms as T
from torch.utils.data import DataLoader

import vispy
from vispy.scene import SceneCanvas
from vispy import app

from datasets.reg_dataset import RegDataset
from models.unet import Unet
from models.unet_variants import UnetVariant



class Vis():
    def __init__(self, dataset, model):
        self.index = 0
        self.dataset = dataset
        self.model = model

        self.canvas = SceneCanvas(keys='interactive',
                                show=True,
                                size=(768, 512))
        self.canvas.events.key_press.connect(self._key_press)
        self.canvas.events.draw.connect(self._draw)
        self.canvas.show()

        self.view = self.canvas.central_widget.add_view()
        self.image = vispy.scene.visuals.Image(parent=self.view.scene)
        
        self.update_image()
   


    def update_image(self):
        data = self.dataset[self.index]
        pred = self.model(data["image"].unsqueeze(0))

        # pred_height = torch.exp(pred["height"].detach())
        pred_cls = torch.softmax(pred["cls"], dim = 1).detach()
        pred_height = pred["height"].detach() * 400
        pred_cls = torch.argmax(pred_cls, dim = 1)
        pred_height = pred_height * pred_cls
        
        pred_height = pred_height.squeeze().numpy()
        pred_cls = pred_cls.squeeze().numpy()
        
        image = data["image"].squeeze()[2:5, :, :].permute(1, 2, 0).numpy()
        height = data["orig_height"].squeeze().numpy()
        cls = data["cls"].squeeze().numpy()
        
        frame = scale(image)
        height_img = self.get_colored_img(height)
        cls_img = np.zeros(frame.shape)
        cls_img[:, :, 0] = cls

        pred_height_img = self.get_colored_img(pred_height)
        pred_cls_img = np.zeros(frame.shape)
        pred_cls_img[:, :, 0] = pred_cls
        
        img = np.concatenate((frame, cls_img, height_img), 1)       
        img_pred = np.concatenate((frame, pred_cls_img, pred_height_img), 1)
        im = np.concatenate((img, img_pred), 0)
           
        self.canvas.title = str(self.index)
        self.image.set_data(im)
        self.view.camera = vispy.scene.PanZoomCamera(aspect=1)
        self.view.camera.set_range()
        self.view.camera.flip = (0, 1, 0)
        self.canvas.update()


    def get_colored_img(self, height):
        mask = height != 0
        disp_map =  height
        # vmax = np.percentile(disp_map[mask], 95)
        # vmin = np.percentile(disp_map[mask], 5)
        vmax = 30
        vmin = 3
        normalizer = mpl.colors.Normalize(vmin=vmin, vmax=vmax)
        mapper = cm.ScalarMappable(norm=normalizer, cmap='Wistia')
        mask = np.repeat(np.expand_dims(mask,-1), 3, -1)
        colormapped_im = (mapper.to_rgba(disp_map)[:, :, :3] * 255).astype(np.uint8)
        colormapped_im[~mask] = 0
        return colormapped_im / 255


    def _key_press(self, event):
        if event.key == 'Right':
            if self.index < len(self.dataset) - 1:
                self.index += 1
            self.update_image()

        if event.key == 'Left':
            if self.index > 0:
                self.index -= 1
            self.update_image()

        if event.key == 'Q':
            self.destroy()

    def destroy(self):
        # destroy the visualization
        self.canvas.close()
        vispy.app.quit()

    def _draw(self, event):
        if self.canvas.events.key_press.blocked():
            self.canvas.events.key_press.unblock()

    def run(self):
        self.canvas.app.run()

def scale(matrix):
    arr = matrix
    new_arr = ((arr - arr.min()) * (1/(arr.max() - arr.min())))
    return new_arr



if __name__ == "__main__":
    data_file = "/home/sapar/work/data/list/citywise256_test.txt"
    data_location = "/home/sapar/work/data/citywise256"
    with open("/home/sapar/work/unet-v2/configs/base.json", 'r') as f:
        config = json.load(f)

    dataset = RegDataset(data_file, data_location, config["norm"], None)
    #loader = DataLoader(dataset, shuffle=False, batch_size=1)

    model_path = "/home/sapar/experiments/remote_sensing/se_resnet50_256_sliding_400_22-04-2023_1/best_checkpoints/70epoch"
    #model = Unet(config["model"])
    model = UnetVariant(config["model"])
    
    model.load_state_dict(torch.load(model_path, map_location=torch.device('cpu')))
    model.eval()

    
    vis = Vis(dataset, model)
    vis.run()