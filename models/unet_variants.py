import torch
import torch.nn as nn
import segmentation_models_pytorch as smp

from models.unet import Head


class UnetVariant(nn.Module):
    def __init__(self, cfg):
        super().__init__()
        model = smp.Unet(encoder_name = cfg["name"], encoder_weights = None, in_channels = cfg["in_c"])
        self.encoder = model.encoder
        self.decoder = model.decoder
        self.cls_head = Head(in_c = 16, out_c = 2)
        self.reg_head = Head(in_c = 16, out_c = 1)

    def forward(self, x):
        features = self.encoder(x)
        decoder_output = self.decoder(*features)

        cls = self.cls_head(decoder_output)
        reg = self.reg_head(decoder_output)

        return {"cls": cls, "height": reg}

    

if __name__ == "__main__":
    x = torch.rand((1, 6, 64, 64))
    cfg = {"name": "efficientnet-b4", "in_c": 6}
    model = UnetVariant(cfg)
    pred = model(x)
    print(pred["cls"].shape)
    print(pred["height"].shape)