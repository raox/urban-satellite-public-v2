import numpy as np
import os
import math
import tifffile
import cv2

import torch
import torchvision
from torch.utils.data import Dataset
import torchvision.transforms as T
from torch.utils.data import DataLoader

from utils.transforms import RandomRotation, OneOf, Affine, RandomErase

class RegDataset(Dataset):
    def __init__(self, data_file, data_location, norm_config, aug_config):
        self.data_file = data_file
        self.path = data_location
        self.aug_config = aug_config
        self.norm_config = norm_config

        self.create_data_list()
        
        if not self.aug_config is None: 
            self.transforms = self.get_transforms(aug_config)
            self.augment = OneOf(self.transforms, aug_config["p"])

    def __len__(self):
        return len(self.data_list)

    def __getitem__(self, idx):
        index = self.data_list[idx]
        
        image_path = os.path.join(self.path, (index + "_RS.tif"))
        label_path = os.path.join(self.path, (index + "_building.tif"))

        image = np.asarray(tifffile.imread(image_path)).astype(np.float32)
        label = np.asarray(tifffile.imread(label_path)).astype(np.float32)
        
        image = torch.from_numpy(image)
        image = torch.permute(image, (2, 0, 1))
        if not self.norm_config is None:
            norm = T.Normalize(self.norm_config["mean"], self.norm_config["std"])
            image = norm(image)
        image = torch.nan_to_num(image)
        
        label = torch.from_numpy(label).unsqueeze(0)
        cls = torch.zeros(label.shape, dtype= torch.long)
        cls[label > 0] = 1

        #handle 0 cases for log
        log_label = torch.clone(label)
        # log_label[label == 0] = 1
        # log_label = torch.log(log_label)   
        log_label = log_label / 400   

        if not self.aug_config is None:
            image, cls, log_label, label = self.augment([image, cls, log_label, label])
        
        return {"image": image, 
                "height": log_label,
                "cls": cls.squeeze(),
                "orig_height": label}

            

    def get_transforms(self, config):
        transforms = []
        if config["rotation"]["use"]:
            limit_angle = config["rotation"]["limit_angle"]
            transforms.append(RandomRotation(limit_angle))
        if config["affine"]["use"]:
            limit_angle = config["affine"]["limit_angle"]
            limit_translation = config["affine"]["limit_translation"]
            limit_shear = config['affine']["limit_shear"]
            limit_scale = config["affine"]["limit_scale"]
            transforms.append(Affine(limit_angle, limit_translation, limit_shear, limit_scale))
        if config["erase"]["use"]:
            max_pixels = config["erase"]["max_pixels"]
            transforms.append(RandomErase(max_pixels))

        return transforms

    def create_data_list(self):
        data_list = []
        with open(self.data_file, "r") as f:
            for line in f:
                data_list.append(line.strip())
        
        self.data_list = data_list


if __name__ == "__main__":
    data_file = "/home/sapar/work/test.txt"
    data_location = "/home/sapar/work/data/sub"
    dataset = RegDataset(data_file, data_location, None)
    data_loader = DataLoader(dataset, shuffle=True, batch_size=1)

    for data in data_loader:
        # image = data["image"]
        # label = data["label"]
        pass
        # print(image.shape)
        # print(label.shape)
        #break