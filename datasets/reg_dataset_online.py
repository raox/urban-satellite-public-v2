import numpy as np
import os
import math
import tifffile
import cv2

import torch
import torchvision
from torch.utils.data import Dataset
import torchvision.transforms as T
from torch.utils.data import DataLoader

from utils.transforms import RandomRotation, OneOf, Affine, RandomErase

class RegDataset(Dataset):
    def __init__(self, data_location, norm_config, aug_config):
        self.data = data_location
        self.aug_config = aug_config
        self.norm_config = norm_config
        
        if not self.aug_config is None: 
            self.transforms = self.get_transforms(aug_config)
            self.augment = OneOf(self.transforms, aug_config["p"])

    def __len__(self):
        return len(self.data_list)

    def __getitem__(self, idx):
        image = self.data
        
        image = torch.from_numpy(image)
        image = torch.permute(image, (2, 0, 1))
        if not self.norm_config is None:
            norm = T.Normalize(self.norm_config["mean"], self.norm_config["std"])
            image = norm(image)
        image = torch.nan_to_num(image)
        
        label = torch.from_numpy(np.zeros([256,256])).unsqueeze(0)
        log_label = torch.clone(label)
        cls = torch.clone(label)
   


        if not self.aug_config is None:
            image, cls, log_label, label = self.augment([image, cls, log_label, label])
        
        return {"image": image, 
                "height": label,
                "cls": cls.squeeze(),
                "orig_height": label}

            

    def get_transforms(self, config):
        transforms = []
        if config["rotation"]["use"]:
            limit_angle = config["rotation"]["limit_angle"]
            transforms.append(RandomRotation(limit_angle))
        if config["affine"]["use"]:
            limit_angle = config["affine"]["limit_angle"]
            limit_translation = config["affine"]["limit_translation"]
            limit_shear = config['affine']["limit_shear"]
            limit_scale = config["affine"]["limit_scale"]
            transforms.append(Affine(limit_angle, limit_translation, limit_shear, limit_scale))
        if config["erase"]["use"]:
            max_pixels = config["erase"]["max_pixels"]
            transforms.append(RandomErase(max_pixels))

        return transforms


if __name__ == "__main__":
    data_file = "/home/sapar/work/test.txt"
    data_location = "/home/sapar/work/data/sub"
    dataset = RegDataset(data_location, None)
    data_loader = DataLoader(dataset, shuffle=True, batch_size=1)

    for data in data_loader:
        # image = data["image"]
        # label = data["label"]
        pass
        # print(image.shape)
        # print(label.shape)
        #break